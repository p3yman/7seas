/*-----------------------------------------------------------------
- General configs
-----------------------------------------------------------------*/
export default {

  // Header title
  header_title: 'Average Adjusters - Marine surveyors - Claims and recovery agents <br> Marine consultants and brokers',

  // Copyright
  copyright: 'All rights reserved. 2019',

  // Menu
  menu: [

    {
      title: 'Home',
      to: '/',
    },
    {
      title: 'Hull',
      to: '/hull',
    },
    {
      title: 'Cargo',
      to: '/cargo',
    },
    {
      title: 'Our Team',
      to: '/team',
    },
    {
      title: 'Our Clients',
      to: '/clients',
    },
    {
      title: 'Africa',
      children: [
        {
          title: 'Angola',
          to: '/angola',
        },
        {
          title: 'D.R. of the Congo',
          to: '/congo',
        },
        {
          title: 'Morocco',
          to: '/morocco',
        },
      ]
    },
    {
      title: 'Contact',
      to: '/#offices',
    },
    {
      title: 'Terms and Conditions',
      to: '/terms-and-conditions',
    },

  ]

};
